<?php

namespace App;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','slug', 'intro', 'body'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments(){
        return $this->hasMany(Comment::class, 'post_id', 'id');
//        return $this->hasMany(Comment::class)->orderBy('id', 'desc');
    }
}
