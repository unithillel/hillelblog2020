<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function countTotal($amount=null){
        if($amount){
            return round($this->price * $amount);
        }else{
            return round($this->price * $this->pivot->amount);
        }
    }

    public function decreaseStocks($amount){

        if($this->stocks < $amount){
            throw new \Exception('Out of stocks', 410);
        }

        $this->stocks -=  $amount;
        $this->save();
    }
}
