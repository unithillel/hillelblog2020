<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Requests\Post\StorePostRequest;
use Illuminate\Support\Facades\Session;
use App\Post;


class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('posts.index')->with(compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show')->with(compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(StorePostRequest $request)
    {
        Post::create($request->all());
        return redirect('/posts')->with('success', 'Blog post created');
    }

    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function update(Post $post, UpdatePostRequest $request)
    {
        $post->update($request->all());
        return redirect('/posts');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/posts');
    }


}
