<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{


    public function create(){
        $cart = Cart::getCartWithProducts();

        return view('order.create', compact('cart'));
    }

    public function store(Request $request){

        DB::beginTransaction();

        try {
            $order = Order::create([
                'comment' => $request->comment,
                'user_id' => Auth::user()->id
            ]);
        }catch (\Exception $exception){
            DB::rollBack();
            return back()->withErrors(['order'=>'order wasn\'t created']);
        }
        $cart = Cart::getCartWithProducts();
        foreach ($cart as $productArr){
            try {
                $order->products()->attach($productArr['product']->id, ['amount' => $productArr['amount']]);
                $productArr['product']->decreaseStocks($productArr['amount']);
            }catch (\Exception $exception){
                DB::rollBack();
                return back()->withErrors(['product'=>'Product can\'t be added']);
            }
        }
        DB::commit();




//        return redirect('/products')->withCookie('cart', null);
    }
}
