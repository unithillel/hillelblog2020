<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignUpUserRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function signUp(){
        return view('users.sign-up');
    }

    public function store(SignUpUserRequest $request){
        $user = User::create($request->all(['email','name']) + ['password' => bcrypt($request->post('password'))]);
        Auth::login($user);
        return redirect('/');
    }

}
