<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public static function getCartArray()
    {
        $cart = json_decode(request()->cookie('cart'), true);

        if (!is_array($cart)) {
            $cart = [];
        }

        return $cart;
    }

    public static function addProduct(Product $product, $amount){
        $cart = static::getCartArray();

        if(isset($cart[$product->id])){
            $cart[$product->id] += $amount;
        }else{
            $cart[$product->id] = $amount;
        }

        return $cart;
    }

    public static function getCartWithProducts(){
        $cart = static::getCartArray();
        $cartWithProducts = [];

        foreach ($cart as $productId => $amount){
            $cartWithProducts[] = [
                'amount' => $amount,
                'product' =>Product::find($productId)
            ];
        }
        return $cartWithProducts;
    }
}
