<?php

use Illuminate\Support\Facades\Route;
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/check', function(){
    return view('layout.base');
});

/*
Route::get('/posts', "PostsController@index");
Route::get('/posts/create' , "PostsController@create");
Route::get('/posts/{post}', "PostsController@show")->name('post.show');
Route::post('/posts', "PostsController@store");
Route::get('/posts/{post}/edit', "PostsController@edit");
Route::put('/posts/{post}', "PostsController@update");
Route::delete('/posts/{post}', "PostsController@destroy");
*/

Route::resource('posts', "PostsController");

Route::resource("products", "ProductsController");

Route::get('/sign-up', "UsersController@signUp");
Route::post('/sign-up', "UsersController@store");



Route::delete('/logout', "LoginController@destroy");
Route::get('/sign-in', "LoginController@create")->name('login');
Route::post('/sign-in', "LoginController@store");

Route::post('/comment', "CommentController@store");


Route::get('/products', "ProductController@index");
Route::get('/cart', "CartController@index");
Route::post('/cart/{product}', "CartController@store");

Route::middleware('auth')->group(function(){
    Route::get('/order', "OrderController@create");
    Route::post('/order', 'OrderController@store');
});



Route::prefix('admin')->middleware('admin')->group(function(){
    Route::get('dashboard', 'Admin\DashboardController@index');
    Route::get('secret', 'Admin\DashboardController@secret');

    Route::resource('order', 'Admin\OrderController');
});
