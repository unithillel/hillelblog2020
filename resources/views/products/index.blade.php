@extends('layout.base')

@section('jumbotron')

    @if(Session::has('success')):
        <div class="alert alert-success" role="alert">
            {{Session::get('success')}}
        </div>
    @endif

    <h1>Products</h1>
@endsection


@section('content')

    @foreach($products as $product)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                     xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false"
                     role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title>
                    <rect width="100%" height="100%" fill="#55595c"/>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                </svg>
                <div class="card-body">

                    <p> {{ $product->title }} </p>
                    <p>Price: <strong>{{ $product->price }} </strong></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="/posts/{{$product->slug}}" class="btn btn-sm btn-outline-secondary">View</a>
                        </div>

                        <form method="POST" action="/cart/{{$product->slug}}">
                            @csrf
                            <div class="form-group">
                                <label >Amount
                                    <input type="number" class="form-control" name="amount" value="1">
                                </label>
                            </div>
                            <button class="btn btn-success">Add to cart</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection
