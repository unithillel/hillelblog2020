@extends('layout.base')

@section('jumbotron')
    <h1>Order {{$order->id}}:</h1>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li>User name: {{$order->user->name}}  </li>
                    <li>Order Id: {{$order->id}}  </li>
                    <li>Comment: {{$order->comment}}  </li>
                </ul>
            </div>
            <div class="col-md-8">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Product title</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Total</th>
                    </tr>

                    @forelse($order->products as $product)
                        <tr>
                            <th>{{ $product->id}}</th>
                            <th>{{ $product->title}}</th>
                            <th>{{ $product->price}}</th>
                            <th>{{ $product->pivot->amount}}</th>
                            <th>{{ $product->countTotal()}}</th>
                        </tr>
                    @empty

                    @endforelse
                </table>
            </div>
        </div>
    </div>
@endsection
