@extends('layout.base')

@section('jumbotron')
    <h1>Orders:</h1>
@endsection

@section('content')
    <div class="col-md-12">
        <table class="table">
            <tr>
                <th>ID</th>
                <th>User name</th>
                <th>Comment</th>
                <th></th>
            </tr>

            @forelse($orders as $order)
                <tr>
                    <th>{{$order->id}}</th>
                    <th>{{$order->user->name}}</th>
                    <th>{{$order->comment}}</th>
                    <th>
                        <a class="btn btn-warning" href="{{route('order.show', $order->id)}}">View</a>
                    </th>
                </tr>
            @empty

            @endforelse
        </table>
    </div>
@endsection
