@extends('layout.base')

@section('jumbotron')
    <h1>Order:</h1>
@endsection

@section('content')
    <div class="col-md-12">
        @include('partials.errors')
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Product</th>
                <th>Price</th>
                <th>Amount</th>
                <th>TotalPrice</th>
            </tr>

            @forelse($cart as $productData)
                <tr>
                    <td>{{ $productData['product']->id }}</td>
                    <td>{{ $productData['product']->title }}</td>
                    <td>{{ $productData['product']->price }}</td>
                    <td>{{ $productData['amount'] }}</td>
                    <td>{{ $productData['product']->countTotal($productData['amount']) }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">CART IS EMPTY</td>
                </tr>
            @endforelse

        </table>
        <div>
            <form action="/order" method="post">
                @csrf
                <div class="form-group">
                    <label> Place a comment
                        <textarea name="comment" id="" class="form-control"></textarea>
                    </label>
                </div>
                <button class="btn btn-success">Create order!</button>
            </form>
        </div>
    </div>
@endsection
