<ul class="nav">
    <li class="nav-item">
        <a class="nav-link" href="/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/posts">Posts</a>
    </li>

    @guest
        <li class="nav-item">
            <a class="nav-link" href="/sign-up">Sign up</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/sign-in">Sign in</a>
        </li>
    @endguest

    @auth
        <li class="nav-item">
            <a class="nav-link" href="#">Hello, {{Auth::user()->name}}</a>
        </li>

        <li class="nav-item">

            <form action="/logout" method="post">
                @method('delete')
                @csrf
                <button class="btn btn-danger">Logout</button>
            </form>
        </li>
    @endauth

    {{--@if(Auth::check())
        <li class="nav-item">
            <a class="nav-link" href="#">Hello, {{Auth::user()->name}}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/logout">Logout</a>
        </li>
    @else
        <li class="nav-item">
            <a class="nav-link" href="/sign-up">Sign up</a>
        </li>
    @endif--}}
</ul>
