@extends('layout.base')

@section('jumbotron')
    <h1>Create new blog post:</h1>
@endsection

@section('content')
    <div class="col-md-6">

        @include('partials.errors')

        <form action="/posts" method="post">

            @csrf

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" value="{{ old('slug') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="intro">Intro:</label>
                <input type="text" name="intro" id="intro" value="{{ old('intro') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="body">Body:</label>
                <textarea name="body" id="body" class="form-control">{{ old('body') }}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Create</button>
            </div>
        </form>
    </div>
@endsection
