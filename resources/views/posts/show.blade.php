@extends('layout.base')

@section('jumbotron')
    <h1>{{$post->title}}</h1>
    <p>{{ $post->intro }}</p>
@endsection

@section('content')
    <div class="col-md-12">
        <p>{{$post->body}}</p>
    </div>

    <hr>



    @auth
        <h2>Add comment:</h2>

        @include('partials.errors')

        <div class="col-md-12">
            <form action="/comment" method="post">
                @csrf
                <input type="hidden" name="post_id" value="{{ $post->id }}">
                <div class="form-group">
                    <label>Comment:
                        <textarea name="body" class="form-control"></textarea>
                    </label>
                </div>
                <button class="btn btn-success">Post Comment</button>

            </form>
        </div>
    @endauth

    <div class="col-md-12">
        <div class="comments">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li>{{$comment->user->name}} - {{$comment->body}} [{{$comment->created_at->diffForHumans()}}]</li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
