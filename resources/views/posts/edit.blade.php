@extends('layout.base')

@section('jumbotron')
    <h1>Update blog post:</h1>
@endsection

@section('content')
    <div class="col-md-6">

        @include('partials.errors')

        <form action="/posts/{{$post->slug}}" method="post">
            {{-- PUT --}}
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" value="{{ $post->title }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" value="{{ $post->slug }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="intro">Intro:</label>
                <input type="text" name="intro" id="intro" value="{{ $post->intro }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="body">Body:</label>
                <textarea name="body" id="body" class="form-control">{{ $post->body }}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
@endsection
