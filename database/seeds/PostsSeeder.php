<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Php is awesome',
                'slug' => 'php_is_awesome',
                'intro' => 'Php is awesome short description',
                'body' => 'Php is awesome main text',
            ],
            [
                'title' => 'Javascript is awesome',
                'slug' => 'javascript_is_awesome',
                'intro' => 'Javascript is awesome short description',
                'body' => 'Javascript is awesome main text',
            ],
            [
                'title' => 'Html is awesome',
                'slug' => 'html_is_awesome',
                'intro' => 'Html is awesome short description',
                'body' => 'Html is awesome main text',
            ],
        ]);
    }
}
