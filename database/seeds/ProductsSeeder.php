<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Product::class, 15)->create();
        for($x= 0; $x < 10; $x++){
            App\Product::create([
                'title'=> 'Product #'. $x,
                'slug' => 'product_'. $x,
                'price' => 10 + $x,
                'description' => 'This product is number'.$x
            ]);
        }
    }
}
